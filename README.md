# Unified hosts file with base extensions

#### AsusWRT or Asuswrt-Merlin
```bash
# Use the below command to get the hosts

rm /etc/hosts

wget -O /etc/hosts https://gitlab.com/hunter/hosts/raw/master/hosts

rm /jffs/configs/hosts

cp /etc/hosts /jffs/configs/hosts

# Flush DNS cache

killall -SIGHUP dnsmasq
```

#### Linux
```bash
# Use the below command to get the hosts

mv /etc/hosts /etc/hosts.backup

wget -O /etc/hosts https://gitlab.com/hunter/hosts/raw/master/hosts

# Flush DNS cache

# CentOS / Fedora / Red Hat (RHEL)
systemctl restart network

# Debian / Ubuntu
systemctl restart networking

# Dnsmasq DNS server
systemctl restart dnsmasq
```

#### Mac OS X
```bash
# Use the below command to get the hosts

sudo mv /etc/hosts /etc/hosts.backup

sudo wget -O /etc/hosts https://gitlab.com/hunter/hosts/raw/master/hosts

# Flush DNS cache on Mac OS X

sudo dscacheutil -flushcache

sudo killall -HUP mDNSResponder
```

#### Windows
`Windows PowerShell`
```text
# Use the below command to get the hosts

mv %SystemRoot%\System32\drivers\etc\hosts %SystemRoot%\System32\drivers\etc\hosts.backup

wget -O %SystemRoot%\System32\drivers\etc\hosts https://gitlab.com/hunter/hosts/raw/master/hosts

# Flush DNS cache

ipconfig /flushdns
```

